const nodeExternals = require('webpack-node-externals');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const serverConfig = {
  mode: 'development',
  target: 'node',
  //externals: [nodeExternals()],
  devtool: 'inline-source-map',
  entry: {
    webServer: './src/server/webServer.ts',
    'webServer.test': './src/server/webServer.test.ts',
  },
  output: {
    path: __dirname + '/build/server',
    filename: '[name].js'
  },
  resolve: {
    extensions: ['.ts']
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        common: {
          chunks: 'all',
          enforce: true,
          name: 'common'
        }
      }
    }
  },
  module: {
    rules: [{
      test: /\.ts/,
      include: [__dirname + '/server'],
      exclude: /node_modules/,
      use: [
        { loader: 'babel-loader' },
        { loader: 'ts-loader' }
      ]
    }]
  },
  /*plugins: [
    new BundleAnalyzerPlugin()
  ]*/
};

module.exports = [serverConfig];
